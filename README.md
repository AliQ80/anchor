# Anchor

<p align="center"><img width="300" src="https://www.publicdomainpictures.net/pictures/90000/nahled/anchor-clipart.jpg" alt="anchor"></p>



## Introduction

Anchor is a WordPress site setup script for [Laravel Valet](https://github.com/laravel/valet).
Based on [Skipper](https://github.com/josephfusco/skipper) By josephfusco on GitHub.

Among some major differences is the separation of credentials, themes and plugins in a separate json files where you can add or remove plugins or themes without fear of breaking the original code, also credentials being in a separate file is more secure and easier to change if needed.

Next major version release : using YAML instead of json files for better readability and ability to comment.



## Usage

For usage information:
```
anchor --help
```

### Site Creation

The anchor command should only be ran within the parked valet directory.

```
anchor mysite
```

Currently, the above command will do the following:

+ Create a new directory called `mysite`
+ Download & install the latest version of WordPress, including the database. (database name will be `valetwp_mysite`)
+ admin username and password are set in the "anchor_setting.json" file located in the current working directory
+ Remove default plugins (Akismet & Hello Dolly)
+ Remove default themes except Twenty Seventeen and Twenty Nineteen
+ Set front page as "Home"
+ Set posts page as "Blog"
+ Add "Home" & "Blog" to primary menu
+ Discourage search engines
+ Change permalinks to use `/%postname%/`
+ Install & activate plugins from the file "plugins.json" located in the current working directory
+ Install & themes from the file "themes.json" located in the current working directory

Once done, the site will open in your default browser and automatically log you in as the admin user.
